
float angle = PI / 4;
float L = 200;
float m = 10;
float g = 10;

float v = 0.0;
float a = 0.0;



void setup() {
  
  size(900,600);
  frameRate(30);
  
}

void draw() {
 background(255);
 
 stroke(0);
 strokeWeight(2);
 
 translate(450,200);
 
 float x1 = L * sin(angle);
 float y1 = L * cos(angle);
 
 line(0, 0, x1, y1);
 fill(0);
 ellipse(x1, y1, m, m);
 
 a = -1 * g / L * sin(angle);
 
 v += a;               
 angle += v;           
 
}
