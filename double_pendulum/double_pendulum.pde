
float L1 = 200;
float L2 = 200;
float m1 = 10;
float m2 = 10;
float g = 10;

float a1 = PI/2;
float a2 = PI/2;

float x1 = 0;
float y1 = 0;

float x2 = 0;
float y2 = 0;

float a1_v = 0;
float a2_v = 0;

float a1_a = 0.02;
float a2_a = 0.01;

PGraphics layer;

void setup() {
  
  size(900,600);
  frameRate(60);
  
  layer = createGraphics(900,600);
  layer.beginDraw();
  layer.background(255);
  layer.endDraw();
  
 
}

void draw() {
  
 //background(255);
 image(layer,0,0);
 
 stroke(0);
 strokeWeight(2); 
 translate(450,200);
 
 
 float num1 = -g * (2 * m1 + m2) * sin(a1);
  float num2 = -m2 * g * sin(a1-2*a2);
  float num3 = -2*sin(a1-a2)*m2;
  float num4 = a2_v*a2_v*L2+a1_v*a1_v*L1*cos(a1-a2);
  float den = L1 * (2*m1+m2-m2*cos(2*a1-2*a2));
  float a1_a = (num1 + num2 + num3*num4) / den;

  num1 = 2 * sin(a1-a2);
  num2 = (a1_v*a1_v*L1*(m1+m2));
  num3 = g * (m1 + m2) * cos(a1);
  num4 = a2_v*a2_v*L2*m2*cos(a1-a2);
  den = L2 * (2*m1+m2-m2*cos(2*a1-2*a2));
  float a2_a = (num1*(num2+num3+num4)) / den;
 
 
 x1 = L1 * sin(a1);
 y1 = L2 * cos(a1);
 
 line(0, 0, x1, y1);
 fill(0);
 ellipse(x1, y1, m1, m1);
 
 x2 = x1 + L2 * sin(a2);
 y2 = y1 + L2 * cos(a2);
 
 line(x1, y1, x2, y2);
 fill(0);
 ellipse(x2, y2, m2, m2);
 
 a1 += a1_v;
 a2 += a2_v;
 
 a1_v += a1_a;
 a2_v += a2_a;


 layer.beginDraw();
 layer.translate(450,200);
 layer.stroke(0);
 layer.strokeWeight(2);
 layer.point(x2,y2);
 layer.endDraw();

  
}
